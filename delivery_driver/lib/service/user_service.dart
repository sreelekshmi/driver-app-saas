import 'package:deliverydriver/location/get_location.dart';
import 'package:deliverydriver/model/user_model.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:deliverydriver/common/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';




class UserService {
  GetLocation gl = GetLocation();


  String _username;
  String _password;
  String _crmLoginUrl = '/?ng=crmapi/v3/driver/login';
  String _crmProfile  = '/?ng=crmapi/v3/driver/profile';
  String _crmChangePassword = '/?ng=crmapi/v3/salesman/reset_password';


  void setUsername(String username){
    _username = username;

    //print(_username);
  }

  void setPassword(String password){
    _password = password;
    // print(_password);
  }


  Future<bool> login() async {
    // print(kBackOfficeUrl+'users/login');
   // _username = "leo@gmail.com";
   // _password ="123";
    print("back_");


    var response = await http.post(kBackOfficeUrl+'users/login' ,
      body: {
        "username": _username,
        "password": _password,
        "role"    : "Driver"
      },
    );


    var jsonRespose = convert.jsonDecode(response.body);
    print(jsonRespose);
    if(jsonRespose['status']=='success' &&  jsonRespose['error']==false){
      SharedPreferences prefs = await SharedPreferences.getInstance();
      int index = jsonRespose['user']['data']['loginArrayIndex'];
      prefs.setString('baseUrl', jsonRespose['user']['company'][index]['baseurl']);

      bool status=await _crmLogin();

      return status;
    }
    else{
      return  false;//Future<bool>.value(false);
    }

  }

  Future <bool> _crmLogin()  async {
    SharedPreferences prefs = await SharedPreferences.getInstance();


    String baseUrl = prefs.getString('baseUrl');
    print(baseUrl+_crmLoginUrl+kCrmUrlParameter);
    var response = await http.post(baseUrl+_crmLoginUrl+kCrmUrlParameter, body: {
      "username": _username,
      "password": _password
    },
    );
    var jsonResponse = convert.jsonDecode(response.body) ;
    print(jsonResponse);



    if(jsonResponse['status']=="success" && jsonResponse['error']==false)
    {
      print(jsonResponse['driver']['ful lname']);
      prefs.setString('fullname', jsonResponse['driver']['fullname']);
      prefs.setString('email', jsonResponse['driver']['username']);
      prefs.setString('phonenumber', jsonResponse['driver']['phonenumber']);
      prefs.setString('userid', jsonResponse['driver']['id']);
      prefs.setBool('isLoggedIn', true);
      return true;
    }
    else{
      return false;


    }
    //  return  Employee.fromJson(jsonResonse);*/
  }

   // Future<List<UserModel>> fetchUser() async {
  // Future<List<UserModel>> fetchUser() async {
     Future<UserModel> fetchUser() async {


       SharedPreferences prefs = await SharedPreferences.getInstance();

      String baseUrl = prefs.getString('baseUrl');
      String userID = prefs.getString('userid');
      var response = await http.post(
        baseUrl + _crmProfile + kCrmUrlParameter, body: {
        "userid": userID
      },
      );

      var jsonRespose = convert.jsonDecode(response.body);
      var user = jsonRespose['response'] ;

     // print(user[0]);
     return UserModel.fromJSON(user[0]);
      //return deliveryChallan.map((order) => Order.fromJSON(order)).toList();

    }

  Future <bool> updatePassword(String currentPassword, String newPassword)  async {
    //print("chaning the "+orderId +" status to "+deliveryStatus);
    //print(sign);
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String baseUrl = prefs.getString('baseUrl');
    String userID = prefs.getString('userid');
    print("Userid"+userID);
    print(baseUrl+_crmChangePassword+kCrmUrlParameter);
    var response = await http.post(baseUrl+_crmChangePassword+kCrmUrlParameter, body: {
      "userid":userID ,
      "current_password": currentPassword,
      "new_password": newPassword
    },
    );

    var jsonResponse = convert.jsonDecode(response.body) ;
    print(jsonResponse);

    if(jsonResponse['status']=="success" && jsonResponse['errors']==false)
    {
      print("okkk");
      return true;
    }
    else{
      print(":werew");
      return false;

    }
    //  return  Employee.fromJson(jsonResonse);*/
  }

      Future <dynamic> resetPasswrd(String currentPassword, String newPassword) async{

        // Shared Preferences
        SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

        var userId = sharedPreferences.get("userid");
        var resetPassUrl = "http://crm.meganemarine.com.sg/?ng=crmapi/v3/salesman/reset_password&api_key=w1o3sdsfvo4eandux30mdr3b70hb4a1fozmd267j";

        var response = await http.post(resetPassUrl, body: {
          "userid":userId ,
          "current_password": currentPassword,
          "new_password": newPassword
        },
        );


        var jsonResponse = convert.jsonDecode(response.body) ;
        print(jsonResponse);


//        http.Response response = await http.post(
//            resetPassUrl,
//            body: {
//              "userid" : userId.toString(),
//              "current_password" : currentPassword,
//              "new_password" : newPassword
//              // "userid" : userId.toString(),
//              // "current_password" : 123,
//              // "new_password" : "seagate88"
//            }
//
//        );
        print(response);

      /*  var webResponse = await json.decode(response.body);

        setState(() {
          print(webResponse);
          // isLoading = false;
          _resetScaffold.currentState.showSnackBar(new SnackBar(content: new Text(webResponse["message"])));
        });*/
      }





  }