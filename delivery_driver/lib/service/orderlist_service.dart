import 'package:intl/intl.dart';
import 'package:deliverydriver/model/order.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:deliverydriver/common/constants.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:deliverydriver/location/get_location.dart';

class OrderListService {
  String _crmOrderList = '/?ng=crmapi/v3/driver/orderlist';
  String _crmOrderStatusChange  = '/?ng=crmapi/v3/driver/delivery_status_change';
  String _crmOrderSignUpload = '/?ng=crmapi/v3/driver/upload_sgin';
  GetLocation gl = GetLocation();


  Future<List<Order>> fetchOrder(var currentDate, String delivery_status) async {

  //  var now = new DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd');
    String formattedDate = formatter.format(currentDate);
    //print(formatted);;
    //print(formatDate(DateTime(currentDate), [yyyy, '-', mm, '-', dd]));

    SharedPreferences prefs = await SharedPreferences.getInstance();

    String baseUrl = prefs.getString('baseUrl');
    String userID = prefs.getString('userid');
    var response;
    if (delivery_status==""){
     response = await http.post(
        baseUrl + _crmOrderList + kCrmUrlParameter, body: {
        "userid": userID,
        'date': formattedDate,
      },
      );
    }
    else{
       response = await http.post(
        baseUrl + _crmOrderList + kCrmUrlParameter, body: {
        "userid": userID,
        'date': formattedDate,
        'delivery_status': delivery_status
      },
      );
    }

    var jsonRespose = convert.jsonDecode(response.body);
    var deliveryChallan = jsonRespose['deliverychellan'] as List;
    //print(deliveryChallan[0]['customer_detail']);

    if(deliveryChallan!=null) {
      return deliveryChallan.map((order) => Order.fromJSON(order)).toList();
    }


  }
  Future<Order> fetchOrderById(String id) async {

    print("Invoice- id"+id);
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String baseUrl = prefs.getString('baseUrl');
    String userID = prefs.getString('userid');
    var response = await http.post(
      baseUrl + _crmOrderList + kCrmUrlParameter, body: {
      "userid": userID,
      "id":id
    },
    );

    var jsonRespose = convert.jsonDecode(response.body);
    var deliveryChallan = jsonRespose['deliverychellan'];

    if(jsonRespose['status']=="success" && jsonRespose['error']==false) {

      print(deliveryChallan[0]);
      return Order.fromJSON(deliveryChallan[0]);
    }
    else {
      //return Order.fromJSON();
      return Order.fromJSON({

        "id": "",
        "userid": "5",
        "account": "WQ",
        "title": "",
        "cn": "",
        "invoicenum": "",
        "date": "2020-05-18",
        "duedate": "2020-05-18",
        "datepaid": "2020-05-18 03:12:29",
        "subtotal": "144.58",
        "discount_type": "p",
        "discount_value": "0.00",
        "discount": "0.00",
        "credit": "0.00",
        "taxname": "",
        "tax": "0.00",
        "tax2": "0.00",
        "tax_total": "0.0000",
        "total": "144.58",
        "taxrate": "0.00",
        "taxrate2": "0.00",
        "status": "Unpaid",
        "paymentmethod": "",
        "notes": "<p>This default invoice term</p>",
        "vtoken": "6508725983",
        "ptoken": "4662134770",
        "r": "0",
        "nd": "2020-05-18",
        "eid": "0",
        "ename": "",
        "vid": "0",
        "quote_id": "0",
        "ticket_id": "0",
        "currency": "1",
        "currency_iso_code": "SGD",
        "currency_symbol": "",
        "currency_prefix": null,
        "currency_suffix": null,
        "currency_rate": "1.0000",
        "recurring": "0",
        "recurring_ends": null,
        "last_recurring_date": null,
        "source": null,
        "sale_agent": "0",
        "last_overdue_reminder": null,
        "allowed_payment_methods": null,
        "billing_street": null,
        "billing_city": null,
        "billing_state": null,
        "billing_zip": null,
        "billing_country": null,
        "shipping_street": null,
        "shipping_city": null,
        "shipping_state": null,
        "shipping_zip": null,
        "shipping_country": null,
        "q_hide": "0",
        "show_quantity_as": "Qty",
        "pid": "0",
        "is_credit_invoice": "0",
        "aid": "1",
        "aname": null,
        "receipt_number": "",
        "updated_at": "2020-05-18 03:12:29",
        "created_at": "2020-05-18 03:02:53",
        "workspace_id": "0",
        "parent_id": "0",
        "c1": "1",
        "c2": "2020-05-19",
        "c3": "1.3342829",
        "c4": "103.8895604",
        "c5": "",
        "signature_data_source": null,
        "signature_data_base64": null,
        "signature_data_svg": null,
        "is_same_state": "1",
        "code": null,
        "delivery_status": "Unassigned",
        "customer_detail": {
          "id": "5",
          "account": "WQ",
          "fname": "",
          "lname": "",
          "company": "",
          "business_number": "",
          "jobtitle": "",
          "cid": "0",
          "o": "1",
          "phone": "",
          "fax": "",
          "email": "",
          "username": null,
          "address": "30 Tai Seng Street",
          "city": "#09-03 BreadTalk IHQ Building",
          "state": "",
          "zip": "534013",
          "country": "Singapore",
          "balance": "0.00",
          "status": "Active",
          "notes": "",
          "options": null,
          "tags": "",
          "token": "",
          "ts": "",
          "img": "",
          "web": "",
          "facebook": "",
          "google": "",
          "linkedin": "",
          "twitter": null,
          "skype": null,
          "tax_number": null,
          "entity_number": null,
          "currency": "1",
          "pmethod": null,
          "autologin": null,
          "lastlogin": null,
          "lastloginip": null,
          "stage": null,
          "timezone": null,
          "isp": null,
          "lat": "1.3342829",
          "lon": "103.8895604",
          "gname": "",
          "gid": "0",
          "sid": null,
          "role": null,
          "country_code": null,
          "country_idd": null,
          "signed_up_by": null,
          "signed_up_ip": null,
          "dob": null,
          "ct": null,
          "assistant": null,
          "asst_phone": null,
          "second_email": null,
          "second_phone": null,
          "taxexempt": null,
          "latefeeoveride": null,
          "overideduenotices": null,
          "separateinvoices": null,
          "disableautocc": null,
          "billingcid": "0",
          "securityqid": "0",
          "securityqans": null,
          "cardtype": null,
          "cardlastfour": null,
          "cardnum": null,
          "startdate": null,
          "expdate": null,
          "issuenumber": null,
          "bankname": null,
          "banktype": null,
          "bankcode": null,
          "bankacct": null,
          "gatewayid": "0",
          "language": null,
          "pwresetkey": null,
          "emailoptout": null,
          "created_at": "2020-04-23 13:21:53",
          "updated_at": null,
          "pwresetexpiry": null,
          "is_email_verified": "0",
          "is_phone_veirifed": "0",
          "photo_id_type": null,
          "photo_id": null,
          "type": "Customer",
          "drive": "15876193132j0bp46jt9r2",
          "workspace_id": "0",
          "parent_id": "0",
          "code": "CUS-00007",
          "display_name": null,
          "secondary_email": "",
          "secondary_phone": null
        },
        "order_detail": [
          {
            "id": "15",
            "invoiceid": "5",
            "userid": "5",
            "type": "",
            "relid": "0",
            "itemcode": "2",
            "tax_code": "",
            "description": "<p>Face Serum</p>",
            "qty": "1",
            "amount": "21.58",
            "taxed": "0",
            "tax_name": null,
            "tax_rate": "0.000",
            "taxamount": "0.00",
            "tax1": "0.0000",
            "tax2": "0.0000",
            "tax3": "0.0000",
            "discount_type": "p",
            "discount_amount": "0.0000",
            "total": "21.58",
            "duedate": "2020-05-18",
            "paymentmethod": "",
            "notes": "",
            "business_id": null,
            "created_at": null,
            "updated_at": null
          },
          {
            "id": "14",
            "invoiceid": "5",
            "userid": "5",
            "type": "",
            "relid": "0",
            "itemcode": "3",
            "tax_code": "",
            "description": "<p>Basic Plan</p>",
            "qty": "1",
            "amount": "123.00",
            "taxed": "0",
            "tax_name": null,
            "tax_rate": "0.000",
            "taxamount": "0.00",
            "tax1": "0.0000",
            "tax2": "0.0000",
            "tax3": "0.0000",
            "discount_type": "p",
            "discount_amount": "0.0000",
            "total": "123.00",
            "duedate": "2020-05-18",
            "paymentmethod": "",
            "notes": "",
            "business_id": null,
            "created_at": null,
            "updated_at": null
          }
        ],
        "driver_detail": [],
        "isyourpackage": "no",
        "history": [],
        "office_latitude": "1.4624",
        "office_longitude": "103.8125"
      });


    }
    //return deliveryChallan.map((order) => Order.fromJSON(order)).toList();

  }

  Future <bool> changeOderStatus(deliveryStatus, orderId)  async {
    //print("chaning the "+orderId +" status to "+deliveryStatus);
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String baseUrl = prefs.getString('baseUrl');
    String userID = prefs.getString('userid');
    print(baseUrl+_crmOrderStatusChange+kCrmUrlParameter);
     await  gl.getLocation();

    var response = await http.post(baseUrl+_crmOrderStatusChange+kCrmUrlParameter, body: {
      "userid":userID ,
      "delivery_id": orderId,
      "status": deliveryStatus,
      "latitude": gl.position.latitude.toString(),
      "longitude": gl.position.longitude.toString()
      },
    );
    var jsonResponse = convert.jsonDecode(response.body) ;
print(jsonResponse);


    if(jsonResponse['status']=="success" && jsonResponse['error']==false)
    {
      return true;
    }
    else
      {
      return false;

    }
    //  return  Employee.fromJson(jsonResonse);*/
  }



  Future <bool> uploadSignature(sign, orderId)  async {
    //print("chaning the "+orderId +" status to "+deliveryStatus);
    print(sign);
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String baseUrl = prefs.getString('baseUrl');
    String userID = prefs.getString('userid');
    print(baseUrl+_crmOrderSignUpload+kCrmUrlParameter);
    //print(orderId); return true;
    var response = await http.post(baseUrl+_crmOrderSignUpload+kCrmUrlParameter, body: {
      "userid":userID ,
      "delivery_id": orderId,
      "sign": 'data:image/png;base64,'+sign
    },
    );

    var jsonResponse = convert.jsonDecode(response.body) ;
    print(jsonResponse);

    if(jsonResponse['status']=="success" && jsonResponse['error']==false)
    {
      print("okkk");
      return true;
    }
    else{
      print(":werew");
      return false;

    }
    //  return  Employee.fromJson(jsonResonse);*/
  }
}