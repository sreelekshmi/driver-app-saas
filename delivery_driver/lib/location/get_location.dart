import 'package:geolocator/geolocator.dart';

class GetLocation
{
  Position position;

  Future<void> getLocation() async{
    Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.bestForNavigation);
    this.position = position;
    print(this.position.latitude.toString());
  }

  Future<void> checkLocationEnabled() async{
    Geolocator geolocator = Geolocator()..forceAndroidLocationManager = true;

    GeolocationStatus geolocationStatus  = await Geolocator().checkGeolocationPermissionStatus();
    print(geolocationStatus);
    if(geolocationStatus==GeolocationStatus.denied){

    }

  }
}