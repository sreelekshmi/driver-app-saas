class Order {
  final String  id;
  final String  account;
  final String invoicenum;
  final String address;
  final String city;
   String deliveyStatus;
   final List orderItems;
   final String phone;
   final List history;
   String sign;
   final isYourPackage;
   String startingLatitude;
   String startingLongitude;

  String orderLatitude;
  String orderLongitude;


  var cutomer_detail;
  Order({this.id, this.account, this.invoicenum, this.address, this.city, this.deliveyStatus, this.orderItems, this.phone, this.history, this.sign, this.isYourPackage, this.startingLatitude, this.startingLongitude, this.orderLatitude, this.orderLongitude});

  Order.fromJSON(Map<String, dynamic> parsedJson)
  : id = parsedJson['id'],
    account = parsedJson['account'],
    invoicenum  = parsedJson['invoicenum']+parsedJson['cn'],
    address = parsedJson['customer_detail']['address'],
    city = parsedJson['customer_detail']['city'],
    cutomer_detail = parsedJson['customer_detail']['address'],
    deliveyStatus = parsedJson['delivery_status'],
    orderItems = parsedJson['order_detail'] as List,
    phone = parsedJson['customer_detail']['phone'],
    history =  parsedJson['history'] as List,
    sign  = parsedJson['signature_data_base64'],
    isYourPackage = parsedJson['isyourpackage'],
    startingLatitude = parsedJson['office_latitude'],
    startingLongitude = parsedJson['office_longitude'],
    orderLatitude = parsedJson['c3'],
    orderLongitude = parsedJson['c4']


  ;
}