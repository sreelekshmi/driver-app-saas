class UserModel {
  final String id;
  final String  name;
  final String  email;
  final String  phonenumber;
  final String role;
  final String vehicleNumber;
  final String vehicleType;
  final int assigned;
  final int pending;
  final int completed;

  UserModel({this.id, this.name, this.email, this.phonenumber, this.role, this.vehicleNumber, this.vehicleType, this.assigned, this.completed, this.pending});
  UserModel.fromJSON(Map<String, dynamic> parsedJson)
      : id = parsedJson['id'],
        name = parsedJson['fullname'],
        email = parsedJson['username'],
        phonenumber = parsedJson['phonenumber'],
        role  = parsedJson['role'],
        vehicleNumber = parsedJson['vehicle_number'],
        vehicleType = parsedJson['vehicle_type'],
        pending = parsedJson['pending_order'],
        completed =parsedJson['successful'],
        assigned = parsedJson['assigned_order']
  ;
}