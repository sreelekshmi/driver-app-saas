import 'package:deliverydriver/screens/today_order_screen.dart';
import 'package:flutter/material.dart';
import 'package:deliverydriver/screens/login_screen.dart';
import 'package:deliverydriver/screens/dashboard_screen.dart';
import 'package:deliverydriver/screens/notfound_screen.dart';
import 'package:deliverydriver/utils/uidata.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


void main() => runApp(Driver());

class Driver extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,

      theme: ThemeData(
        primaryColor: Colors.blue,
        textTheme : TextTheme(
          bodyText2: TextStyle(
            fontFamily: 'Roboto',
          ),
        ),

      ),
      // home: LoginScreen(),
      initialRoute: LoginScreen.id,
      routes: {
        // WelcomeScreen.id: (context) => WelcomeScreen(),
        LoginScreen.id: (context) => LoginScreen(),
        DashboardScreen.id: (context) => DashboardScreen(),
        NotFoundScreen.id: (context) => NotFoundScreen(),
        TodayOrderScreen.id:(context) =>TodayOrderScreen(),
        //OrderDetailScreen.id: (context) => OrderDetailScreen(),

        //  MyApp.id :(context) => MyApp()
        //ScanScreen.id: (context) => ScanScreen(),
        //  RegistrationScreen.id: (context) => RegistrationScreen()

      },
        onUnknownRoute: (RouteSettings rs) => new MaterialPageRoute(
        builder: (context) => NotFoundScreen(
          appTitle: UIData.coming_soon,
          icon: FontAwesomeIcons.solidSmile,
          title: UIData.coming_soon,
          message: "Under Development",
          iconColor: Colors.green,
        )),
    );
  }
}

