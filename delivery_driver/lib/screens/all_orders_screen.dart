import 'package:deliverydriver/screens/order_detail_screen.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:deliverydriver/service/orderlist_service.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:deliverydriver/model/order.dart';



class AllOrderScreen extends StatelessWidget {
  static const String id = 'todays_order_screen';

  final OrderListService orderListService = OrderListService();
  DateTime currentDate = new DateTime.now();
  AllOrderScreen(bool isCurrentDate,[ var currentDate]){
    if(isCurrentDate==true){
      print(currentDate);
      this.currentDate = currentDate;
      print(this.currentDate);
    }

  }
  @override
  Widget build(BuildContext context) {
    return FutureProvider(
      create: (context)=>orderListService.fetchOrder(currentDate, ""),
      catchError: (context, error) {
        print(error.toString());
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text("Task"),
          actions: <Widget>[
            IconButton(
              icon: Icon(FontAwesomeIcons.calendar),
              onPressed: () {
                DatePicker.showDatePicker(context,
                    showTitleActions: true,
                    //  minTime: DateTime(2018, 3, 5),
                    // maxTime: DateTime(2019, 6, 7),
                    onChanged: (date) {
                      print('change $date');
                    },
                    onConfirm: (date) {
                      print('confirm $date');
                      Navigator.pop(context);
                      Navigator.push(context, MaterialPageRoute(builder: (context)=> AllOrderScreen(true,date)));


                    },

                    currentTime: DateTime.now(), locale: LocaleType.en);
              },

            )
          ],
        ),
        // drawer: CommonDrawer(),
        body: BodyLayout(),
      ),


    );
  }
}


class BodyLayout extends StatefulWidget {
  @override
  _BodyLayoutState createState() => _BodyLayoutState();
}


class _BodyLayoutState extends State<BodyLayout>   {

  bool _loading = false;


  @override

  List<String> orderStatus = List<String>();

  final OrderListService order = OrderListService();

  Widget build(BuildContext context) {

    List<Order> orders = Provider.of<List<Order>>(context);
    List<String> selectedItemValue = List<String>();


    return !_loading? orders==null?Center(child: CircularProgressIndicator()):
    orders.length>0?
    (ListView.builder(
      itemCount: orders.length,
      itemBuilder: (context,index){
        // selectedDeliveryStatus.add(value)

        for (int i = 0; i < orders.length; i++) {
          orderStatus.add(orders[i].deliveyStatus);
        }
        String orderSt = orders[index].deliveyStatus;
        return Card(
          child: InkWell(
            onTap: (){
              //print(orders[index].id);
              Navigator.push(context, MaterialPageRoute(builder: (context) => OrderDetailScreen(orderId:orders[index].id, invoiceNum:orders[index].invoicenum)));

            },
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Row(

                children: <Widget>[
                  Flexible(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,

                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(

                            children: <Widget>[

                              CircleAvatar(
                                radius: 10.0,
                                child: Icon(
                                  FontAwesomeIcons.fileInvoice,
                                  size: 10.0,
                                ),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Text(orders[index].invoicenum),
                              SizedBox(
                                width: 20,
                              ),

                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(

                            children: <Widget>[

                              CircleAvatar(
                                radius: 10.0,
                                child: Icon(
                                  FontAwesomeIcons.user,
                                  size: 10.0,
                                ),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Text(orders[index].account)
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(

                            children: <Widget>[

                              CircleAvatar(
                                radius: 10.0,
                                child: Icon(
                                  FontAwesomeIcons.mapMarker,
                                  size: 10.0,
                                ),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Text(orders[index].address)
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(

                            children: <Widget>[

                              CircleAvatar(
                                radius: 10.0,
                                child: Icon(
                                  FontAwesomeIcons.city,
                                  size: 10.0,
                                ),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Expanded(
                                child: Text(orders[index].city, maxLines: null,
                                  softWrap: true,
                                  overflow: TextOverflow.visible,
                                ),
                              )
                            ],
                          ),
                        ),
                      ],

                    ),
                  )


                ],
              ),
            ),
          ),
          /*child: ListTile(
            title: Text(orders[index].invoicenum),
            subtitle: Text(orders[index].account),
            trailing: Text(orders[index].cutomer_detail),
          ),*/
        );
      },

    )
    ):Center(child: Text("No Delivery for the selected Date",    textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 20.0,

          fontWeight: FontWeight.w300, // light
          fontStyle: FontStyle.italic, // italic
        )),):Center(child:CircularProgressIndicator());
  }
}



List<DropdownMenuItem<String>> _dropDownItem() {
  List<String> ddl = ["Unassigned", "Acknowledged", "Assigned", "Started", "Inprogress", "Successful"];
  return ddl
      .map((value) => DropdownMenuItem(
    value: value,
    child: Text(value),
  ))
      .toList();
}
