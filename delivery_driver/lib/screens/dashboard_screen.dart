import 'package:deliverydriver/screens/widgets/common_drawer.dart';
import 'package:deliverydriver/service/orderlist_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:deliverydriver/screens/dashboard/dashboard_menu_row.dart';
import 'package:deliverydriver/screens/widgets/login_background.dart';
import 'package:deliverydriver/screens/widgets/profile_tile.dart';
import 'package:deliverydriver/utils/uidata.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:barcode_scan/barcode_scan.dart';


class DashboardScreen extends StatefulWidget {
  static const String id = 'dashboard_screen';

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {

  bool _loading = false;
  final OrderListService order = OrderListService();

  Size deviceSize;
  String _fullname;
  String  _email;

  Future<Null> getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _fullname = prefs.getString("fullname");
      _email = prefs.getString("email");
    });
  }

  @override
  void initState() {
    super.initState();
    _fullname="";
    getSharedPrefs();
    setState(() {

    });

  }
  Widget appBarColumn(BuildContext context) => SafeArea(

    child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4.0, vertical: 18.0),
      child: new Column(
        children: <Widget>[
          new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[


               ProfileTile(
                title: _fullname,
                subtitle: "Welcome to the Driver App",
                textColor: Colors.white,
              ),

            ],
          ),
        ],
      ),
    ),
  );

  Widget searchCard() => Padding(
    padding: const EdgeInsets.all(8.0),
    child: Card(
      elevation: 2.0,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Icon(Icons.search),
            SizedBox(
              width: 10.0,
            ),
            Expanded(
              child: TextField(
                decoration: InputDecoration(
                    border: InputBorder.none, hintText: "Enter the delivery code"),
                onSubmitted: (value) async{


                  _loading = true;
                  //orderStatus[index] = value;

                  var orderDetail = await order.fetchOrderById(value);
                  print(orderDetail);
                  setState(() {
                    _loading = false;
                  });
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      // return object of type Dialog
                      return AlertDialog(
                        title:  orderDetail.id==""?  Text("Error"):Text("Package - " + orderDetail.invoicenum),

                        content:  orderDetail.id==""? Text("Barcode Not found"): (orderDetail.isYourPackage == "yes" ? Text(
                            "This is your package") : Text(
                            "This is not your package")),
                        actions: <Widget>[
                          // usually buttons at the bottom of the dialog
                          new FlatButton(
                            child: Text("Close"),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    },
                  );
                },
                textInputAction: TextInputAction.search,
              ),
            ),
            IconButton(
              icon: Icon(Icons.camera_alt),
              tooltip: 'Scan Barcode',
              onPressed: () async {
                //print("navigate")
                var result = await BarcodeScanner.scan();
                print(result
                    .type); // The result type (barcode, cancelled, failed)
                print(result.rawContent); // The barcode content
                print(result.format); // The barcode format (as enum)
                print(result
                    .formatNote); // If a unknown format was scanned this field contains a note

                _loading = true;
                //orderStatus[index] = value;

                var orderDetail = await order.fetchOrderById(result.rawContent);
                setState(() {
                  _loading = false;
                });
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    // return object of type Dialog
                    return AlertDialog(
                      title:  orderDetail.id==""?  Text("Error"):Text("Package - " + orderDetail.invoicenum),

                      content:  orderDetail.id==""? Text("Barcode Not found"): (orderDetail.isYourPackage == "yes" ? Text(

                      "This is your package") : Text(
                          "This is not your package")),
                      actions: <Widget>[
                        // usually buttons at the bottom of the dialog
                        new FlatButton(
                          child: Text("Close"),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    );
                  },
                );
              }
            )
          ],
        ),
      ),
    ),
  );

  Widget actionMenuCard() => Padding(
    padding: const EdgeInsets.symmetric(horizontal: 8.0),
    child: Card(
      elevation: 2.0,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          //crossAxisAlignment: CrossAxisAlignment.,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
         // mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            DashboardMenuRow(
              firstIcon: FontAwesomeIcons.tasks,
              firstLabel: "Today's Order     ",
              firstIconCircleColor: Colors.blue,
              secondIcon: FontAwesomeIcons.creativeCommonsSamplingPlus,
              secondLabel: "Completed\nOrders",
              secondIconCircleColor: Colors.orange,

            ),
            DashboardMenuRow(
              firstIcon: FontAwesomeIcons.phone,
              //firstLabel: "All Orders",
              firstLabel: "Emergency\nCall",
              firstIconCircleColor: Colors.red,
              secondIcon: FontAwesomeIcons.user,
              secondLabel: "Profile",
              secondIconCircleColor: Colors.teal,

            ),
            DashboardMenuRow(
              firstIcon: FontAwesomeIcons.stackExchange,
              firstLabel: "Password",
              firstIconCircleColor: Colors.cyan,
              secondIcon: FontAwesomeIcons.signOutAlt,
              secondLabel: "Logout",
              secondIconCircleColor: Colors.redAccent,

            ),
          ],
        ),
      ),
    ),
  );

  Widget balanceCard() => Padding(
    padding: const EdgeInsets.all(8.0),
    child: Card(
      elevation: 2.0,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Balance",
                  style: TextStyle(fontFamily: UIData.ralewayFont),
                ),
                Material(
                  color: Colors.black,
                  shape: StadiumBorder(),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "500 Points",
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: UIData.ralewayFont),
                    ),
                  ),
                )
              ],
            ),
            Text(
              "₹ 1000",
              style: TextStyle(
                  fontFamily: UIData.ralewayFont,
                  fontWeight: FontWeight.w700,
                  color: Colors.green,
                  fontSize: 25.0),
            ),
          ],
        ),
      ),
    ),
  );

  Widget allCards(BuildContext context) => SingleChildScrollView(
    child: Column(
      children: <Widget>[
        appBarColumn(context),
        SizedBox(
          height: deviceSize.height * 0.01,
        ),
        searchCard(),
        SizedBox(
          height: deviceSize.height * 0.01,
        ),
         _loading?CircularProgressIndicator():SizedBox(),
        actionMenuCard(),
        SizedBox(
          height: deviceSize.height * 0.01,
        ),
        //balanceCard(),
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;





    return Scaffold(
      appBar: AppBar(
        title: Text('Dashboard'),
          automaticallyImplyLeading:false
      ),
   //   drawer: CommonDrawer(),

      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          LoginBackground(
            showIcon: false,
          ),
          allCards(context),
        ],
      ),
    );
  }
}

