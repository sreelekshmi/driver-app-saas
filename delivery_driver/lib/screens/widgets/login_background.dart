import 'package:flutter/material.dart';
import 'package:deliverydriver/screens/tools/arc_clipper.dart';
import 'package:deliverydriver/utils/uidata.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LoginBackground extends StatelessWidget {
  final showIcon;
  final image;
  LoginBackground({this.showIcon = true, this.image});

  Widget topHalf(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;
    return new Flexible(
      flex: 2,
      child: ClipPath(
        clipper: new ArcClipper(),
        child: Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                  gradient: new LinearGradient(
                colors: UIData.kitGradients,
              )),
            ),
            showIcon
                ? new Center(
                    child: SizedBox(
                        height: deviceSize.height / 8,
                        width: deviceSize.width / 2,
                          child: CircleAvatar(
                            radius: 10.0,
                            child: Icon(
                              FontAwesomeIcons.lock,
                              size: 30.0,
                            ),
                          ),

//                        child: FlutterLogo(
//                          colors: Colors.yellow,
//                        )
                    ),
                  )
                : new Container(
                    width: double.infinity,
                    child: image != null
                        ? Image.network(
                            image,
                            fit: BoxFit.cover,
                          )
                        : new Container())
          ],
        ),
      ),
    );
  }

  final bottomHalf = new Flexible(
    flex: 3,
    child: new Container(),
  );

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[topHalf(context), bottomHalf],
    );
  }
}
