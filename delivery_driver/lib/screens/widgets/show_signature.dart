import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:deliverydriver/service/orderlist_service.dart';

class ShowSignature extends StatefulWidget {
  SignatureData signatureData;
   ShowSignature(this.signatureData);
  @override
  _ShowSignatureState createState() => _ShowSignatureState();
}

class _ShowSignatureState extends State<ShowSignature> {

  bool _loading = false;
  final OrderListService orderListService = OrderListService();
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title: Text("Customer Signature"),
      ),
      body: Center(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: <Widget>[
                Container(
                    color: Colors.grey[300], child: Image.memory(base64.decode(widget.signatureData.sign))
                ),
                SizedBox(
                  width: 20.0,
                ),
                Divider(
                  color: Colors.black,
                ),
               // if(widget.showSubmitButton==false)
                !_loading?MaterialButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    // side: BorderSide(color: Colors.red)
                  ),
                  height: 40.0,
                  minWidth: 70.0,

                  color: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  child: Text("Submit"),
                  onPressed: () async  {
                    _loading = true;
                    String signImage = widget.signatureData.sign;
                    String order = widget.signatureData.orderId;
                    var  orderSignatureUpload =  await orderListService.uploadSignature(signImage, order);
                    print(orderSignatureUpload);
                    if(orderSignatureUpload == true){
                      setState(()  {
                        _loading = false;
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            // return object of type Dialog
                            return AlertDialog(
                              title: new Text("Signature"),
                              content: new Text("Signature is uploaded Successfully"),
                              actions: <Widget>[
                                // usually buttons at the bottom of the dialog
                                new FlatButton(
                                  child: new Text("Close"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          },
                        );

                      });

                    }
                    else {

                      setState(()  {
                        _loading = false;
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            // return object of type Dialog
                            return AlertDialog(
                              title: new Text("Signature"),
                              content: new Text("An Error occured in current order. Please contact Admin"),
                              actions: <Widget>[
                                // usually buttons at the bottom of the dialog
                                new FlatButton(
                                  child: new Text("Close"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          },
                        );

                      });


                    }


                  },
                  splashColor: Colors.lightBlueAccent,
                ):CircularProgressIndicator(),


              ],
            ),
          )),
    );
  }
}

class SignatureData{
  String orderId;
  String sign;
  void setOrderId(String orderId){
    this.orderId = orderId;

  }
  void setSign(String sign){
    this.sign = sign;
  }
}
