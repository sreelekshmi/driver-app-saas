import 'package:deliverydriver/screens/today_order_screen.dart';
import 'package:flutter/material.dart';
import 'package:deliverydriver/utils/uidata.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../all_orders_screen.dart';
import '../change_password_screen.dart';
import '../completed_order_screen.dart';
import '../profile_screen.dart';
import 'package:url_launcher/url_launcher.dart';


class LabelBelowIcon extends StatelessWidget {
  final label;
  final IconData icon;
  final iconColor;
  final onPressed;
  final circleColor;
  final isCircleEnabled;
  final betweenHeight;

  LabelBelowIcon(
      {this.label,
      this.icon,
      this.onPressed,
      this.iconColor = Colors.white,
      this.circleColor,
      this.isCircleEnabled = true,
      this.betweenHeight = 5.0});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()  async {  // => onPressed,

        print("i am presses");
        if(label=="Today's Order     "){
         // Navigator.push(context, MaterialPageRoute(builder: (context) => TodaysOrderScreen()));
           Navigator.push(context, MaterialPageRoute(builder: (context) => TodayOrderScreen()));


        }
        else if(label=='Completed\nOrders'){
          Navigator.push(context, MaterialPageRoute(builder: (context) => CompletedOrderScreen(false)));
        }
//        else if(label=='All Orders'){
//          Navigator.push(context, MaterialPageRoute(builder: (context) => AllOrderScreen(false)));
//        }
        else if(label=='Emergency\nCall'){
          launch(('tel://6582918173'));

          // Navigator.push(context, MaterialPageRoute(builder: (context) => AllOrderScreen(false)));
        }
        else if(label=='Profile'){
          Navigator.push(context, MaterialPageRoute(builder: (context) => ProfileScreen()));
        }
        else if(label=='Password'){
          Navigator.push(context, MaterialPageRoute(builder: (context) => ChangePasswordScreen()));
        }
        else if(label=='Logout'){
          Navigator.pop(context);
          SharedPreferences preferences = await SharedPreferences.getInstance();
          await preferences.clear();

        }
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          isCircleEnabled
              ? CircleAvatar(
                  backgroundColor: circleColor,
                  radius: 20.0,
                  child: Icon(
                    icon,
                    size: 12.0,
                    color: iconColor,
                  ),
                )
              : Icon(
                  icon,
                  size: 23.0,
                  color: iconColor,
                ),
          SizedBox(
            height: betweenHeight,
          ),
          Text(
            label,
            textAlign: TextAlign.center,
            style: TextStyle(fontFamily: UIData.ralewayFont),
          )
        ],
      ),
    );
  }
}
