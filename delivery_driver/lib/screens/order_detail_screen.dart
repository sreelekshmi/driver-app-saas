import 'package:deliverydriver/model/order.dart';
import 'package:deliverydriver/screens/customer_signature_screen.dart';
import 'package:deliverydriver/screens/map_screen.dart';
import 'package:deliverydriver/screens/product_description_screen.dart';
import 'package:deliverydriver/screens/signature_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:deliverydriver/screens/widgets/common_drawer.dart';
import 'package:deliverydriver/service/orderlist_service.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';

class OrderDetailScreen extends StatelessWidget {
  static const String id = 'order_detail_screen';
  final String orderId;
  final String invoiceNum;
  final String startingLatitude;
  final String startingLongitude;
  final String orderLatitude;
  final String orderLongitude;
  final OrderListService  orderListService = OrderListService();
//  OrderDetailScreen orderDetailScreen = OrderDetailScreen();
  OrderDetailScreen({@required this.orderId, @required this.invoiceNum, @required this.startingLatitude, @required this.startingLongitude, @required this.orderLatitude, @required this.orderLongitude});
  final List<int> colorCodes = <int>[600, 500, 100];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Order Detail:- "+invoiceNum),

      ),
    //  drawer: CommonDrawer(),
      body: FutureProvider(
        create: (context) => orderListService.fetchOrderById(orderId),
        catchError: (context, error) {
          print(error.toString());
        },
        child: Center(

          child: Consumer<Order>(
            builder: (context, order, widget){
             // return(CircularProgressIndicator());
              return(order !=null )? ListView(
                children: ListTile.divideTiles(
                  context: context,
                  tiles: [
                    ListTile(
                      title: Text('Customer Details',
                      style: TextStyle(fontWeight: FontWeight.bold),),

                    ),
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(15,  8, 8, 8),
                          child: Row(
                            children: <Widget>[

                              CircleAvatar(
                                radius: 10.0,
                                child: Icon(
                                  FontAwesomeIcons.fileInvoice,
                                  size: 10.0,
                                ),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Text(order.account),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(15,  8, 8, 8),
                          child: Row(
                            children: <Widget>[

                              CircleAvatar(
                                radius: 10.0,
                                child: Icon(
                                  FontAwesomeIcons.phone,
                                  size: 10.0,
                                ),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Text(order.phone),
                            ],
                          ),
                        )
                      ],
                    ),
                    ListTile(
                      title: Text('Address',
                        style: TextStyle(fontWeight: FontWeight.bold),),

                    ),
                    ListTile(
                            leading:CircleAvatar(
                                     radius: 10.0,
                                        child: Icon(
                                    FontAwesomeIcons.mapMarker,
                                    size: 10.0,
                                    ),
                                    ),
                      title: Text(order.address,
                                  maxLines: null,),
                      trailing: Icon(Icons.keyboard_arrow_right),
                      onTap: (){
                          //    print(startingLatitude);
                        print(startingLatitude +','+ startingLongitude +','+ orderLatitude +','+ orderLongitude);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => MapScreen(startingLatitude: startingLatitude, startingLongitude: startingLongitude, orderLatitude: orderLatitude, orderLongitude: orderLongitude)));
                       // Navigator.push(context, MaterialPageRoute(builder: (context) => MapScreen()));
                       //Navigator.push(context, MaterialPageRoute(builder: (context) => GooglemapNavigationScreen()));

                      },

                    ),

                    ListTile(
                      leading: CircleAvatar(
                                radius: 10.0,
                                child: (
                                Icon(FontAwesomeIcons.fileInvoice,
                                size: 10.0,
                                )
                                ),
                      ),

                      title: Text('Task Description',
                        style: TextStyle(fontWeight: FontWeight.bold),),

                    ),
                    for(int i=0; i<(order.orderItems.length); i++)
//                      Padding(
//                        padding: EdgeInsets.fromLTRB(0,  0, 0, 0),
//                        child: Container(
//                          color: Colors.lightBlueAccent[colorCodes[2]],
//
//                          child: Row(
//
//                            mainAxisAlignment: MainAxisAlignment.spaceAround,
//                            crossAxisAlignment: CrossAxisAlignment.end,
//                            children: <Widget>[
//
//                              CircleAvatar(
//                                radius: 10.0,
//                                child: Text(('#').toString()),
//                              ),
//
//                              Text(order.orderItems[i]['description']),
//
//                              // HtmlWidget(order.orderItems[i]['description']),
//
//                                Text(" - "+order.orderItems[i]['qty']),
//                            ],
//                          ),
//                        ),
//                      ),

                      Container(
                       // height: 40.0,
                        color: Colors.white70,
                        child: ListTile(
                       //   hoverColor: Colors.lightBlue,

                       //   contentPadding: EdgeInsets.fromLTRB(40, 0, 20, 0),
                          leading:CircleAvatar(
                            radius: 10.0,
                            child: Icon(
                              FontAwesomeIcons.tasks,
                              size: 10.0,
                            ),
                          ),
                         // title: HtmlWidget("wrr"),
                         title: Text(order.orderItems[i]['description']),
                          subtitle: Text("x "+order.orderItems[i]['qty']+" (Qty)"),
                          trailing: Icon(Icons.keyboard_arrow_right),

                          //trailing: Text("x "+order.orderItems[i]['qty']+" (Qty)"),
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ProductDescriptionScreen(order.orderItems[i]['html_description'])));

                          },


                        ),
                      ) ,

                    ListTile(
                      leading:CircleAvatar(
                        radius: 10.0,
                        child: Icon(
                          FontAwesomeIcons.pen,
                          size: 10.0,
                        ),
                      ),
                      title: Text("Signature",  style: TextStyle(fontWeight: FontWeight.bold),),
                      trailing: Icon(Icons.keyboard_arrow_right),
                      onTap: (){
                //print(order.sign);
                      try {
                        if (order.sign.isNotEmpty) {
                          // print("not");
                          String content = order.sign;
                          content = content.replaceAll("data:image/png;base64,", "");
                          Navigator.push(context, MaterialPageRoute(
                              builder: (context) =>
                                  CustomerSignatureScreen(content)));
                        }
                      }
                        catch(e){
                        Navigator.push(context, MaterialPageRoute(
                        builder: (context) =>
                        SignatureScreen(orderID: order.id)));
                        }

                        }

                       /* else {
                      /   //print("wrwe");


                        }
                      },*/

                    ),

                    ListTile(
                      title: Text('Delivery History',
                        style: TextStyle(fontWeight: FontWeight.bold),),

                    ),
                    for(int i=0; i<(order.history.length); i++)
                      Padding(
                        padding: EdgeInsets.fromLTRB(15,  8, 8, 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text(order.history[i]['delivery_status']+ " at "),

                            Text(order.history[i]['date_time']),
                          ],
                        ),
                      ),

//                     MaterialButton(
//                       shape: RoundedRectangleBorder(
//                           borderRadius: BorderRadius.circular(18.0),
//                          // side: BorderSide(color: Colors.red)
//                       ),
//                      height: 40.0,
//                      minWidth: 70.0,
//
//                      color: Theme.of(context).primaryColor,
//                      textColor: Colors.white,
//                      child: new Text("Start"),
//                      onPressed: ()  {
//                         print("Wr");
//                        print("erwer");
//                      },
//                      splashColor: Colors.lightBlueAccent,
//                    )


                  ]
                ).toList(),
              ): CircularProgressIndicator();

            },
          ),
        ),
      )
    );

  }
}







