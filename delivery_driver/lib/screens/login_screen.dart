import 'dart:io';

import 'package:flutter/material.dart';
import 'package:deliverydriver/components/rounded_button.dart';
import 'package:deliverydriver/common/constants.dart';
import 'package:deliverydriver/screens/dashboard_screen.dart';
import 'package:deliverydriver/service/user_service.dart';
import 'package:deliverydriver/location/get_location.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:android_intent/android_intent.dart';
import 'package:shared_preferences/shared_preferences.dart';





class LoginScreen extends StatefulWidget {
  static  const String id = "login_screen" ;



  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  PermissionStatus _permissionStatus = PermissionStatus.undetermined;
  void initState() {
    super.initState();
    requestLocationPermission();
    _gpsService();
    autoLogIn();
  }

  void autoLogIn() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final isLoggedIn = prefs.getBool('isLoggedIn');

    if (isLoggedIn != null) {
      if(isLoggedIn==true){
        Navigator.push(context, MaterialPageRoute(builder: (context) => DashboardScreen()));

      }
    }
  }





/*Checking if your App has been Given Permission*/
  Future<bool> requestLocationPermission({Function onPermissionDenied}) async {

    Map<Permission, PermissionStatus> statuses = await [
      Permission.location,
     // Permission.storage,
    ].request();
    print(statuses[Permission.location]);
    if (await Permission.speech.isPermanentlyDenied) {
      // The user opted to never again see the permission request dialog for this
      // app. The only way to change the permission's status now is to let the
      // user manually enable it in the system settings.
      openAppSettings();
    }

  }
/*Show dialog if GPS not enabled and open settings location*/
  Future _checkGps() async {
    if (!(await Geolocator().isLocationServiceEnabled())) {
      if (Theme.of(context).platform == TargetPlatform.android) {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("Can't get gurrent location"),
                content:const Text('Please make sure you enable GPS and try again'),
                actions: <Widget>[
                  FlatButton(child: Text('Ok'),
                      onPressed: () {
                        final AndroidIntent intent = AndroidIntent(
                            action: 'android.settings.LOCATION_SOURCE_SETTINGS');
                        intent.launch();
                        Navigator.of(context, rootNavigator: true).pop();
                        _gpsService();
                      })],
              );
            });
      }
    }
  }

/*Check if gps service is enabled or not*/
  Future _gpsService() async {
    if (!(await Geolocator().isLocationServiceEnabled())) {
      _checkGps();
      return null;
    } else
      return true;
  }







  _getThingsOnStartup() async{
    Geolocator geolocator = Geolocator()..forceAndroidLocationManager = true;

    GeolocationStatus geolocationStatus  = await Geolocator().checkGeolocationPermissionStatus();
    print(geolocationStatus);
    if(geolocationStatus==GeolocationStatus.denied){

    }
  }

    @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        //padding: const EdgeInsets.symmetric(horizontal: 24.0),

        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Hero(
                tag: 'logo',
                child: Container(
                  height: 100.0,
                  child: Image.asset('images/logo.png'),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),

              Center(
                  child: Text(
                      "Driver | For Delivery",
                       style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0, color: Colors.lightBlue),
                  )),
              SizedBox(
                height: 20.0,
              ),
              LoginForm(),
              //SnackBarPage(),

            ],
          ),
        ),
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {

  bool _loading = false;
  final _formKey = GlobalKey<FormState>();
  final userService  = UserService();
  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            TextFormField(
              //controller: TextEditingController(text:"sam@yahoo.com"),

              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter email';
                }
                return null;
              },
              onChanged: (value) {

                userService.setUsername(value);

              },
              decoration: kTextFieldDecoration.copyWith(hintText: 'Enter the email'),

            ),

            SizedBox(
              height: 8.0,
            ),

            TextFormField(
              obscureText: true,
            //  controller: TextEditingController(text:"123"),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter password';
                }
                return null;
              },
              onChanged: (value) {
                userService.setPassword(value);

              },
              decoration: kTextFieldDecoration.copyWith(hintText: 'Enter the Password'),
            ),
            SizedBox(
              height: 24.0,
            ),


            Row(

              mainAxisAlignment: MainAxisAlignment.center,
              //  crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                /*  !_loading ? RaisedButton(
                                   child: Text('Log in'),
                                    onPressed: (){
                                    _loginn();
                                   setState(() => _loading = true);
                                     },
                                   ) : CircularProgressIndicator();*/

                !_loading ? RoundedButton(
                    title: 'Log In',
                    colour: Colors.lightBlue,
                    onPressed: () async{

                      if (_formKey.currentState.validate()) {
                        setState(() => _loading = true);

                        var loginStatus = await userService.login();


                        if(loginStatus == true){
                          print("yes");
                          Navigator.push(context, MaterialPageRoute(builder: (context) => DashboardScreen()));
                          setState(() => _loading = false);
                        }else{
                          setState(() => _loading = false);
                          final snackBar = SnackBar(
                            content: Text('Invalid username or password'),
                            action: SnackBarAction(
                              label: '',
                              onPressed: () {
                                // Some code to undo the change.
                              },
                            ),
                          );

                          // Find the Scaffold in the widget tree and use
                          // it to show a SnackBar.
                          Scaffold.of(context).showSnackBar(snackBar);

                        }

                      }

                      //  userService.login();
                      // Navigator.pushNamed(context, LoginScreen.id);
                      //  Navigator.pushNamed(context, TestScreen.id);
                    }

                ): CircularProgressIndicator(),
              ],
            ),



          ],
        ),
      ),
    );
  }
}

