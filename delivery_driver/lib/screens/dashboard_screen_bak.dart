import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class DashboardScreen extends StatefulWidget {
  static const String id = 'dashboard_screen';
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {



  int _selectedIndex = 0;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Home',
      style: optionStyle,
    ),
    Text(
      'Index 1: Orders',
      style: optionStyle,
    ),
    Text(
      'Index 2:  Enter Your OrderID',
      style: optionStyle,
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard'),
        backgroundColor: Colors.lightBlue,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.notifications,
              color: Colors.white,
            ),
            tooltip: 'Notifications',
            onPressed: () {
              // do something
            },
          ),
          IconButton(
            icon: const Icon(
              Icons.date_range,
              color: Colors.white,
            ),
            tooltip: 'Calendar',
            onPressed: () {

            },
          ),
          IconButton(
            icon: const Icon(
              Icons.more_vert,
              color:Colors.white,
            ),
            tooltip: 'More',
            onPressed: () {

            },
          ),
        ],

      ),
      body: Center(

        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.view_list,color: Colors.lightBlueAccent,),
            title: Text('Orders'),

          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.developer_mode,color: Colors.lightBlueAccent,),
            title: Text('Scan QR Code'),

          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.green,
        onTap: _onItemTapped,
      ),
    );
  }
}

