import 'package:deliverydriver/screens/widgets/common_drawer.dart';
import 'package:deliverydriver/service/orderlist_service.dart';
import 'package:deliverydriver/service/user_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:deliverydriver/screens/dashboard/dashboard_menu_row.dart';
import 'package:deliverydriver/screens/widgets/login_background.dart';
import 'package:deliverydriver/screens/widgets/profile_tile.dart';
import 'package:deliverydriver/utils/uidata.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:barcode_scan/barcode_scan.dart';


class ChangePasswordScreen extends StatefulWidget {
  static const String id = 'dashboard_screen';

  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen>     with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  String phoneNumber, otp;
  bool _loading = false;
  final UserService userService = UserService();

  Size deviceSize;
  String _fullname;
  String  _email;





  @override
  initState() {
    // TODO: implement initState
    super.initState();
    _fullname="";

    controller = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 1500));
    animation = new Tween(begin: 0.0, end: 1.0).animate(
        new CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn));
    animation.addListener(() => this.setState(() {}));
    controller.forward();
  }

  @override
  void dispose() {
    controller?.dispose();

    super.dispose();
  }

  Widget appBarColumn(BuildContext context) => SafeArea(

    child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4.0, vertical: 18.0),
      child: new Column(
        children: <Widget>[
          new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[


              ProfileTile(
                title: _fullname,
                subtitle: "Welcome to the Driver App",
                textColor: Colors.white,
              ),

            ],
          ),
        ],
      ),
    ),
  );

  Widget searchCard() => Padding(
    padding: const EdgeInsets.all(8.0),
    child: Card(
      elevation: 2.0,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Icon(Icons.search),
            SizedBox(
              width: 10.0,
            ),
            Expanded(
              child: TextField(
                decoration: InputDecoration(
                    border: InputBorder.none, hintText: "Enter the delivery code"),
                onSubmitted: (value){

                },
                textInputAction: TextInputAction.search,
              ),
           ),
//            IconButton(
//                icon: Icon(Icons.camera_alt),
//                tooltip: 'Scan Barcode',
//                onPressed: () async {
//                  //print("navigate")
//                  var result = await BarcodeScanner.scan();
//                  print(result
//                      .type); // The result type (barcode, cancelled, failed)
//                  print(result.rawContent); // The barcode content
//                  print(result.format); // The barcode format (as enum)
//                  print(result
//                      .formatNote); // If a unknown format was scanned this field contains a note
//
//                  _loading = true;
//                  //orderStatus[index] = value;
//
//                  var orderDetail = await order.fetchOrderById(result.rawContent);
//                  setState(() {
//                    _loading = false;
//                  });
//                  showDialog(
//                    context: context,
//                    builder: (BuildContext context) {
//                      // return object of type Dialog
//                      return AlertDialog(
//                        title: Text("Package - " + orderDetail.invoicenum),
//
//                        content: orderDetail.isYourPackage == "yes" ? Text(
//                            "This is your package") : Text(
//                            "This is not your package"),
//                        actions: <Widget>[
//                          // usually buttons at the bottom of the dialog
//                          new FlatButton(
//                            child: Text("Close"),
//                            onPressed: () {
//                              Navigator.of(context).pop();
//                            },
//                          ),
//                        ],
//                      );
//                    },
//                  );
//                }
//            )
          ],
        ),
      ),
    ),
  );

  Widget actionMenuCard() => Padding(
    padding: const EdgeInsets.symmetric(horizontal: 8.0),
    child: Card(
      elevation: 2.0,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              DashboardMenuRow(
                firstIcon: FontAwesomeIcons.tasks,
                firstLabel: "Todays Order",
                firstIconCircleColor: Colors.blue,
                secondIcon: FontAwesomeIcons.creativeCommonsSamplingPlus,
                secondLabel: "Completed Orders",
                secondIconCircleColor: Colors.orange,

              ),
              DashboardMenuRow(
                firstIcon: FontAwesomeIcons.images,
                firstLabel: "All Orders",
                firstIconCircleColor: Colors.red,
                secondIcon: FontAwesomeIcons.user,
                secondLabel: "Profile",
                secondIconCircleColor: Colors.teal,

              ),
              DashboardMenuRow(
                firstIcon: FontAwesomeIcons.stackExchange,
                firstLabel: "Password",
                firstIconCircleColor: Colors.cyan,
                secondIcon: FontAwesomeIcons.signOutAlt,
                secondLabel: "Logout",
                secondIconCircleColor: Colors.redAccent,

              ),
            ],
          ),
        ),
      ),
    ),
  );

  Widget balanceCard() => Padding(
    padding: const EdgeInsets.all(8.0),
    child: Card(
      elevation: 2.0,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Balance",
                  style: TextStyle(fontFamily: UIData.ralewayFont),
                ),
                Material(
                  color: Colors.black,
                  shape: StadiumBorder(),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "500 Points",
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: UIData.ralewayFont),
                    ),
                  ),
                )
              ],
            ),
            Text(
              "₹ 1000",
              style: TextStyle(
                  fontFamily: UIData.ralewayFont,
                  fontWeight: FontWeight.w700,
                  color: Colors.green,
                  fontSize: 25.0),
            ),
          ],
        ),
      ),
    ),
  );
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _pass = TextEditingController();
  final TextEditingController _currentPass = TextEditingController();


  Widget allCards(BuildContext context) => SingleChildScrollView(
    child: Padding(
      padding:  EdgeInsets.only(top: deviceSize.height / 2-150),
      child: Column(

        children: <Widget>[
          SizedBox(
            height: deviceSize.height / 2 - 20,
            width: deviceSize.width * 0.85,
            child: new Card(
                color: Colors.white, elevation: 2.0,
            child: Padding(
              padding: const EdgeInsets.all(18.0),
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    TextFormField(
                      obscureText: true,

                    //  onChanged: (phone) => phoneNumber = phone,
                      //enabled: !snapshot.data,
                      controller:_currentPass,
                      validator: (value) {
                        if ((value.trim()).isEmpty) {
                          return 'Please enter current Password';
                        }
                        return null;
                      },
                      style:
                       TextStyle(fontSize: 15.0, color: Colors.black),
                      decoration:  InputDecoration(
                          hintText: "Current Password",

                          labelStyle: TextStyle(fontWeight: FontWeight.w700)),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextFormField(
                      obscureText: true,
                  //  onChanged: (phone) => phoneNumber = phone,
                  //enabled: !snapshot.data,
                      controller: _pass,
                        validator: (value) {
                          if ((value.trim()).isEmpty) {
                            return 'Please enter New Password';
                          }
                          return null;
                        },
                  style:
                  TextStyle(fontSize: 15.0, color: Colors.black),
                  decoration:  InputDecoration(
                      hintText: "New Password",

                      labelStyle: TextStyle(fontWeight: FontWeight.w700)),
              ),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextFormField(
                      obscureText: true,
                      //  onChanged: (phone) => phoneNumber = phone,
                      //enabled: !snapshot.data,
                        validator: (value) {
                          if ((value.trim()).isEmpty || (_pass.text!=value.trim())) {
                            return 'Password Not Matching';
                          }

                          return null;
                        },
                      style:
                      TextStyle(fontSize: 15.0, color: Colors.black),
                      decoration:  InputDecoration(
                          hintText: "Confirm Password",

                          labelStyle: TextStyle(fontWeight: FontWeight.w700)),
                    ),
                    SizedBox(height: 30.0,),
                    !_loading==true?Material(
                      elevation: 10.0,
                      color: Colors.transparent,
                      shape: const StadiumBorder(),
                      child: InkWell(
                        onTap: () async{
                           if (_formKey.currentState.validate()) {
                             var  updatePassword =  await userService.updatePassword((_currentPass.text).trim(), (_pass.text).trim());
                               print(updatePassword);
                             if(updatePassword == true){
                               setState(()  {
                                 _loading = false;
                                 print("changed");

                               });

                             }
                             else {
                               setState(() => _loading = false);
                              print("error ocuured invalid");

                               // Find the Scaffold in the widget tree and use
                               // it to show a SnackBar.
                              // Scaffold.of(context).showSnackBar(snackBar);
                             }

                             showDialog(
                               context: context,
                               builder: (BuildContext context) {
                                 // return object of type Dialog
                                 return AlertDialog(
                                   title: Text("Password"),

                                   content: updatePassword == true ? Text(
                                       "Password Updated Successfully") : Text(
                                       "Current Password not Matching"),
                                   actions: <Widget>[
                                     // usually buttons at the bottom of the dialog
                                     new FlatButton(
                                       child: Text("Close"),
                                       onPressed: () {
                                          Navigator.of(context).pop();
                                          _formKey.currentState.reset();
                                       },
                                     ),
                                   ],
                                 );
                               },
                             );


                           }
                        },
                        splashColor: Colors.lightBlueAccent,
                        child: Ink(
                          height: 50.0,
                          decoration: ShapeDecoration(
                              shape: const StadiumBorder(),
                              gradient: LinearGradient(
                                colors: UIData.kitGradients,
                              )),
                          child: Center(
                            child: Text(
                              "Submit",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 20.0),
                            ),
                          ),
                        ),
                      ),
                    ):CircularProgressIndicator()

                  ],
                  ),
                ),
              ),
            ),
              ),
          ),
         // actionMenuCard(),

          //balanceCard(),
        ],
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Change Password'),
      ),
      //   drawer: CommonDrawer(),

      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          LoginBackground(
            showIcon: true,
          ),


          allCards(context),
        //  loginCard(context)

        ],
      ),
    );
  }

  Widget loginCard(BuildContext context) {
    return Opacity(
      opacity: animation.value,
      child: SizedBox(
        height: deviceSize.height / 2 - 20,
        width: deviceSize.width * 0.85,
        child: new Card(
            color: Colors.white, elevation: 2.0),
      ),
    );
  }

}

