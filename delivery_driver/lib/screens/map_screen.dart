import 'package:flutter/material.dart';
import 'dart:async';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mapbox_navigation/library.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:deliverydriver/common/constants.dart';



class MapScreen extends StatefulWidget {
  String startingLatitude, startingLongitude , orderLatitude, orderLongitude;
  MapScreen({this.startingLatitude, this.startingLongitude, this.orderLatitude, this.orderLongitude});

  @override
  _MapScreen createState() => _MapScreen();
}

class _MapScreen extends State<MapScreen> {
  GoogleMapController mapController;
  Map<MarkerId, Marker> markers = {};

  Map<PolylineId, Polyline> polylines = {};
  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();
  String googleAPiKey = kGoogleAPIKey;



  BitmapDescriptor sourceIcon;
  BitmapDescriptor destinationIcon;
  String _platformVersion = 'Unknown';
//  Location _origin =  Location(name: "City Hall", latitude: 42.886448, longitude: -78.878372);
//  Location _destination = Location(name: "Downtown Buffalo", latitude: 42.8866177, longitude: -78.8814924);
  WayPoint _origin = WayPoint(name: "City Hall", latitude: 42.886448, longitude: -78.878372);
  WayPoint _destination = WayPoint(name: "Downtown Buffalo", latitude: 42.8866177, longitude: -78.8814924);

  MapboxNavigation _directions;
  bool _arrived = false;
  double _distanceRemaining, _durationRemaining;


//   LatLng source = LatLng(9.2506,76.5401);
//   LatLng destination = LatLng(9.1748,76.5013 );
  LatLng source;
  LatLng destination;
  double _originLatitude = 9.2385, _originLongitude = 76.5315;
  double _destLatitude =  9.6741, _destLongitude = 76.3401;
  @override
  void initState() {
    super.initState();
    initPlatformState();
    print("init -here");
    print(widget.startingLatitude+','+widget.startingLongitude);

    source =  LatLng(double.parse(widget.startingLatitude), double.parse(widget.startingLongitude));
    destination =   LatLng(double.parse(widget.orderLatitude), double.parse(widget.orderLongitude));
    _origin = WayPoint(name: "singapore", latitude: double.parse(widget.startingLatitude),longitude:  double.parse(widget.startingLongitude));
    _destination = WayPoint(name: "singapore", latitude: double.parse(widget.orderLatitude),longitude:  double.parse(widget.orderLongitude));

    print("init-orgin");
    print(_origin.latitude.toString()+','+_origin.longitude.toString());
    print("init-destination");
    print(_destination.latitude.toString()+','+_destination.longitude.toString());

    //print(_origin.latitude);
    //print(_origin.longitude);
       _originLatitude = _origin.latitude;
     _originLongitude = _origin.longitude;

    _destLatitude = _destination.latitude;
    _destLongitude = _destination.longitude;

    /// origin marker
    _addMarker(LatLng(_originLatitude, _originLongitude), "origin",
        BitmapDescriptor.defaultMarker);

    /// destination marker
    _addMarker(LatLng(_destLatitude, _destLongitude), "destination",
        BitmapDescriptor.defaultMarkerWithHue(90));
    _getPolyline();

    setSourceAndDestinationIcons();


  }

  void setSourceAndDestinationIcons() async {
    sourceIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), 'images/driving_pin.png');
    destinationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5),
        'images/destination_map_marker.png');
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    _directions = MapboxNavigation(onRouteProgress: (arrived) async {
      _distanceRemaining = await _directions.distanceRemaining;
      _durationRemaining = await _directions.durationRemaining;

      setState(() {
        _arrived = arrived;
      });
      if (arrived)
      {
        await Future.delayed(Duration(seconds: 3));
        await _directions.finishNavigation();
      }
    });

    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await _directions.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    setState(() {
      _platformVersion = platformVersion;
    });
  }




  void _onMapCreated(GoogleMapController controller) async {
    mapController = controller;
  }

  _addMarker(LatLng position, String id, BitmapDescriptor descriptor) {
    MarkerId markerId = MarkerId(id);
    Marker marker =
    Marker(markerId: markerId, icon: descriptor, position: position);
    markers[markerId] = marker;
  }

  _addPolyLine() {
    PolylineId id = PolylineId("poly");
    Polyline polyline = Polyline(
        polylineId: id, color: Colors.red, points: polylineCoordinates);
    polylines[id] = polyline;
    setState(() {});
  }

  _getPolyline() async {
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
        googleAPiKey,
        PointLatLng(_originLatitude, _originLongitude),
        PointLatLng(_destLatitude, _destLongitude),
        travelMode: TravelMode.driving,
       // wayPoints: [PolylineWayPoint(location: "Mavelikara, Cherthala")]
    );

    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }
    _addPolyLine();
  }

/*
  void onMapCreated(GoogleMapController controller) {
    controller.setMapStyle(Utils.mapStyles);
    _controller.complete(controller);
    setMapPins();
    setPolylines();
  }

  void setMapPins() {
    setState(() {
      // source pin
      _markers.add(Marker(
          markerId: MarkerId('sourcePin'),
          position: source,
          icon: sourceIcon
         )
      );
      // destination pin
      _markers.add(Marker(
          markerId: MarkerId('destPin'),
          position: destination,
          icon: destinationIcon
         )
      );
    });
  }

  setPolylines() async {

    PolylinePoints polylinePoints = PolylinePoints();
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(googleAPiKey,
        _originLatitude, _originLongitude, _destLatitude, _destLongitude);
    print(result.points);
//    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(googleAPIKey, source.latitude, source.longitude,
//        destination.latitude,
//        destination.longitude);
    //(result); return;
    if (result.isNotEmpty) {
      // loop through all PointLatLng points and convert them
      // to a list of LatLng, required by the Polyline
      result.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }

    setState(() {
      // create a Polyline instance
      // with an id, an RGB color and the list of LatLng pairs
      Polyline polyline = Polyline(
          polylineId: PolylineId("poly"),
          color: Color.fromARGB(255, 40, 122, 198),
          points: polylineCoordinates);

      // add the constructed polyline as a set of points
      // to the polyline set, which will eventually
      // end up showing up on the map
      _polylines.add(polyline);
    });

  }


*/
  @override
  Widget build(BuildContext context) {


    return Scaffold(
        appBar: AppBar(
          title: Text('Map'),
        ),


        body: GoogleMap(
          initialCameraPosition: CameraPosition(
              target: LatLng(_originLatitude, _originLongitude), zoom: 15),
          myLocationEnabled: true,
          tiltGesturesEnabled: true,
          compassEnabled: true,
          scrollGesturesEnabled: true,
          zoomGesturesEnabled: true,
          onMapCreated: _onMapCreated,
          markers: Set<Marker>.of(markers.values),
          polylines: Set<Polyline>.of(polylines.values),
          mapType: MapType.terrain

        ),
//      body: GoogleMap(
//        markers: _markers,
//        polylines: _polylines,
//        mapType: MapType.normal,
//        initialCameraPosition: CameraPosition(target: source,zoom: 13.0) ,
//       onMapCreated: onMapCreated,
//       /* onMapCreated: (GoogleMapController controller) {
//          _controller.complete(controller);
//        },*/
//      ),

      floatingActionButton: FloatingActionButton.extended(
        onPressed: () async {
          print(_destination.latitude.toString());
              print(_origin);
              if(destination.longitude!= _origin.longitude && destination.latitude!=_origin.latitude) {
            await _directions.startNavigation(
                origin: _origin,
                destination: _destination,
                mode: MapBoxNavigationMode.driving,
                simulateRoute: true,
                language: "German",
                units: VoiceUnits.metric);
              }
              else {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    // return object of type Dialog
                    return AlertDialog(
                      title: Text("Navigation Alert"),


                      content:  Text("Your are currently at destination point"),
                      actions: <Widget>[
                        // usually buttons at the bottom of the dialog
                        new FlatButton(
                          child: Text("Close"),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    );
                  },
                );

              }

        },
        label: Text('Give the direction'),
        icon: Icon(Icons.directions_boat),
      ),

    );



  }
}

class Utils {
  static String mapStyles = '''[
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]''';
}