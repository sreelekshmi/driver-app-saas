import 'package:deliverydriver/screens/order_detail_screen.dart';
import 'package:deliverydriver/screens/widgets/common_drawer.dart';
import 'package:deliverydriver/service/orderlist_service.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:deliverydriver/model/order.dart';


class TodayOrderScreen extends StatelessWidget {
  static const String id = 'todays_order_screen';
  final OrderListService orderListService = OrderListService();
  var currentDate = new DateTime.now();
  @override
  Widget build(BuildContext context) {
    return FutureProvider(
      create: (context)=>orderListService.fetchOrder(currentDate, ""),
      catchError: (context, error) {
        print(error.toString());
      },

      child: Scaffold(
       appBar: AppBar(
         title: Text("Today's Task"),
       ),
      // drawer: CommonDrawer(),
       body: BodyLayout(),
     ),


    );
  }
}


class BodyLayout extends StatefulWidget {
  @override
  _BodyLayoutState createState() => _BodyLayoutState();
}


class _BodyLayoutState extends State<BodyLayout>   {

  bool _loading = false;
  String startingLatitude, startingLongitude;

  @override

  List<String> orderStatus = List<String>();

  final OrderListService order = OrderListService();

  Widget build(BuildContext context) {

    List<Order> orders = Provider.of<List<Order>>(context);
    List<String> selectedItemValue = List<String>();


    return !_loading? orders==null?Center(child: CircularProgressIndicator()):
    orders.length>0?(ListView.builder(
      itemCount: orders.length,

    itemBuilder: (context,index){
       // selectedDeliveryStatus.add(value)
      for (int i = 0; i < orders.length; i++) {
        orderStatus.add(orders[i].deliveyStatus);
      }
      String orderSt = orders[index].deliveyStatus;
        return Card(
          child: InkWell(
            onTap: (){
              //print( orders[index].orderLatitude);
              if(index==0){
                startingLatitude = orders[index].startingLatitude;
                startingLongitude = orders[index].startingLongitude;

              }
              else {
                startingLatitude = orders[index-1].orderLatitude;
                startingLongitude = orders[index-1].orderLongitude;

              }
//              Navigator.push(context, MaterialPageRoute(builder: (context) => OrderDetailScreen(orderId:orders[index].id, invoiceNum:orders[index].invoicenum,  startingLatitude: startingLatitude, startingLongitude: startingLongitude, orderLatitude: orders[index].orderLongitude, orderLongitude: orders[index].orderLongitude)));
                Navigator.push(context, MaterialPageRoute(builder: (context)=> OrderDetailScreen(orderId: orders[index].id, invoiceNum: orders[index].invoicenum, startingLatitude: startingLatitude, startingLongitude: startingLongitude, orderLatitude: orders[index].orderLatitude, orderLongitude: orders[index].orderLongitude)));
            },
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Row(

                children: <Widget>[
                  Flexible(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,

                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(

                            children: <Widget>[

                              CircleAvatar(
                                radius: 10.0,
                                child: Icon(
                                  FontAwesomeIcons.fileInvoice,
                                  size: 10.0,
                                ),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Text(orders[index].invoicenum),
                              SizedBox(
                                width: 20,
                              ),
                              Container(
                                height: 30.0,
                               // color: Colors.white,
                                padding: EdgeInsets.fromLTRB(10.0, 0.0,10.0, 0.0),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                    gradient: LinearGradient(colors: [Color(0xff374ABE), Color(0xff64B6FF)],
                                      begin: Alignment.centerLeft,
                                      end: Alignment.centerRight,
                                    ),
                                    borderRadius: BorderRadius.circular(10.0)
                                ),
                                child: DropdownButton(
                                  elevation: 16,
                                  dropdownColor: Colors.lightBlue,
                                  focusColor: Colors.white,
                                  hint: Text("Delivery Status"),
                                //  isExpanded: true,


                                  style: TextStyle(
                                    color: Colors.black
                                  ),
                                  value: orderStatus[index].toString(),
                                  items: _dropDownItem(),
                                  underline: SizedBox(height: 2.0),

                                  onChanged: (value) async {
                                    setState(() {
                                      _loading = true;
                                    });

                                    orderStatus[index] = value;

                                    var  orderChangeStatus =  await order.changeOderStatus(value, orders[index].id);
                                    if(orderChangeStatus == true){
                                      setState(()  {
                                        _loading = false;
                                        print("hii");
                                        final snackBar = SnackBar(
                                          content: Text('Current order has some error. Please contact admin'),
                                          action: SnackBarAction(
                                            label: '',
                                            onPressed: () {
                                              // Some code to undo the change.
                                            },
                                          ),
                                        );
                                      });
                                    }
                                    else {
                                      setState(() => _loading = false);
                                      final snackBar = SnackBar(
                                        content: Text('Current order has some error. Please contact admin'),
                                        action: SnackBarAction(
                                          label: '',
                                          onPressed: () {
                                            // Some code to undo the change.
                                          },
                                        ),
                                      );

                                      // Find the Scaffold in the widget tree and use
                                      // it to show a SnackBar.
                                      Scaffold.of(context).showSnackBar(snackBar);
                                    }


                                    /*setState(() async {
                                      _loading = true;
                                      var  orderChangeStatus =  await order.changeOderStatus(value, orders[index].id);
                                      if (orderChangeStatus == true){
                                       // setState(() => _loading = false);
                                        _loading = false;

                                      }
                                    });*/
                                  },
                                  //   hint: Text('Select'),
                                  //style: TextStyle(color: Colors.blue),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(

                            children: <Widget>[

                              CircleAvatar(
                                radius: 10.0,
                                child: Icon(
                                  FontAwesomeIcons.user,
                                  size: 10.0,
                                ),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Text(orders[index].account)
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(

                            children: <Widget>[

                              CircleAvatar(
                                radius: 10.0,
                                child: Icon(
                                  FontAwesomeIcons.mapMarker,
                                  size: 10.0,
                                ),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Text(orders[index].address)
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(

                            children: <Widget>[

                              CircleAvatar(
                                radius: 10.0,
                                child: Icon(
                                  FontAwesomeIcons.city,
                                  size: 10.0,
                                ),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Expanded(
                                child: Text(orders[index].city, maxLines: null,
                                    softWrap: true,
                                  overflow: TextOverflow.visible,
                                ),
                              )
                            ],
                          ),
                        ),
                      ],

                    ),
                  )


                ],
              ),
            ),
          ),
          /*child: ListTile(
            title: Text(orders[index].invoicenum),
            subtitle: Text(orders[index].account),
            trailing: Text(orders[index].cutomer_detail),
          ),*/
        );
      },
    )
    ):Center(child: Text("No Delivery for  Today",    textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 20.0,

          fontWeight: FontWeight.w300, // light
          fontStyle: FontStyle.italic, // italic
        )),):Center(child:CircularProgressIndicator());
  }
}


List<DropdownMenuItem<String>> _dropDownItem() {
  //List<String> ddl = ["Unassigned", "Acknowledged", "Assigned", "Started", "Inprogress", "Successful"];
 List<String> ddl = ["Assigned", "Acknowledged", "Started", "Inprogress", "Successful"];
 List<Icon> icon = [Icon(
   FontAwesomeIcons.assistiveListeningSystems,
   size: 10.0,
 ), Icon(
   FontAwesomeIcons.hackerNews,
   size: 10.0,
 ), Icon(
   FontAwesomeIcons.hourglassStart,
   size: 10.0,
 ),
   Icon(
     FontAwesomeIcons.productHunt,
     size: 10.0,
   ), Icon(
     FontAwesomeIcons.lastfmSquare,
     size: 10.0,
   )];

  return ddl
      .map((value) => DropdownMenuItem(

    value: value,
    child:
    Row(
      mainAxisAlignment: MainAxisAlignment.start,

      children: <Widget>[
        icon[ddl.indexOf(value)],
        SizedBox(width: 25.0),
        Text(value),
      ],
    ),
  ))
      .toList();
}
