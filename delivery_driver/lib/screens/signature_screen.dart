import 'dart:convert';
import 'package:deliverydriver/screens/widgets/show_signature.dart';
import 'package:deliverydriver/service/orderlist_service.dart';
import 'package:flutter/material.dart';
import 'package:signature/signature.dart';


class SignatureScreen extends StatefulWidget {
  final String orderID;
  SignatureScreen({@required this.orderID});


  @override
  _SignatureScreenState createState() => _SignatureScreenState();
}

class _SignatureScreenState extends State<SignatureScreen> {

  SignatureData sd = SignatureData();
  final SignatureController _controller = SignatureController(
    penStrokeWidth: 5,
    penColor: Colors.black,
    exportBackgroundColor: Colors.white,
  );

  @override
  void initState() {

    super.initState();
    _controller.addListener(() => print("Value changed"));
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Signature'),
      ),
      //   d

      body: Builder(
        builder: (context) => Scaffold(
          body: ListView(
            children: <Widget>[
//              Container(
//                height: 300,
//                child: Center(
//                  child: Text('Big container to test scrolling issues'),
//                ),
//              ),
              //SIGNATURE CANVAS
              Signature(
                controller: _controller,
                height: 250,
                backgroundColor: Colors.white70,
              ),
              //OK AND CLEAR BUTTONS
              Container(
                decoration: const BoxDecoration(color: Colors.black),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    //SHOW EXPORTED IMAGE IN NEW ROUTE
                    IconButton(
                      icon: const Icon(Icons.check),
                        color: Colors.blue,
                      onPressed: () async {
                        if (_controller.isNotEmpty) {
                          var data = await _controller.toPngBytes();
                          String base64Image = base64Encode(data);
                          sd.setOrderId(widget.orderID);
                          sd.setSign(base64Image);
                         // Provider.of<SignatureData>(context).setSign(base64Image);
                         // Provider.of<SignatureData>(context).setOrderId(widget.orderID);

                          //print(base64Image);
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (BuildContext context) {

                                return ShowSignature(sd);
                                },
                            ),
                          );
                        }
                      },
                    ),
                    //CLEAR CANVAS
                    IconButton(
                      icon: const Icon(Icons.clear),
                      color: Colors.blue,
                      onPressed: () {
                        setState(() => _controller.clear());
                      },
                    ),


                  ],
                ),
              ),
              Container(
                height: 300,
                child: Center(
                  child: Text('Please Sign in the above signature pad'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

