import 'package:deliverydriver/model/user_model.dart';
import 'package:deliverydriver/screens/widgets/profile_tile.dart';
import 'package:deliverydriver/service/user_service.dart';
import 'package:flutter/material.dart';
import 'package:deliverydriver/screens/widgets/profile_one_page.dart';
import 'package:deliverydriver/screens/widgets/common_scaffold.dart';
import 'package:provider/provider.dart';

  /*class ProfileScreen extends StatelessWidget {
  final UserService userService = UserService();
  @override
  Widget build(BuildContext context) {
    return FutureProvider(
      create: (context)=>userService.fetchUser(),
      catchError: (context, error){
        print(error.toString());
      },
      child: MainProfileScreen(),
    );
  }
}*/


class ProfileScreen extends StatelessWidget {
  Size deviceSize;
  static const String id = 'profile_screen';
  final UserService userService = UserService();



  Widget profileHeader(String name, String role) => Container(
    height: deviceSize.height / 4,
    width: double.infinity,
    color: Colors.black,
    child: Padding(
      padding: const EdgeInsets.all(10.0),
      child: Card(
        clipBehavior: Clip.antiAlias,
        color: Colors.black,
        child: FittedBox(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50.0),
                    border: Border.all(width: 2.0, color: Colors.white)),
                child: CircleAvatar(
                  radius: 40.0,
                  backgroundImage:  AssetImage("images/driver-logo.png") ,//AssetImage('images/driver.png'),
                ),
              ),
              Text(
                name,
                style: TextStyle(color: Colors.white, fontSize: 20.0),
              ),
              Text(
                role,
                style: TextStyle(color: Colors.white),
              )
            ],
          ),
        ),
      ),
    ),
  );
  Widget imagesCard() => Container(
    height: deviceSize.height / 6,
    child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Photos",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
            ),
          ),
          Expanded(
            child: Card(
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: 5,
                itemBuilder: (context, i) => Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.network(
                      "https://cdn.pixabay.com/photo/2016/10/31/18/14/ice-1786311_960_720.jpg"),
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );

  Widget profileColumn(String label, String value) => Padding(
    padding: const EdgeInsets.all(8.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[

           Expanded(
             child: Padding(
               padding: const EdgeInsets.all(8.0),
               child: Text(label),
             ),
           ),


        Expanded(
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: Column(
              //  mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                      value,
                    style: TextStyle(
                          fontWeight: FontWeight.bold
                        ),

                  ),
                  SizedBox(
                    height: 5.0,
                  ),

                ],
              ),
            ))
      ],
    ),
  );

  Widget postCard(UserModel user) => Container(
    width: double.infinity,
    height: deviceSize.height / 3,
    child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Details",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
            ),
          ),
          //postCard(user),
          //          profileColumn("Vehicle Number", user.vehicleNumber),
//          profileColumn("Vehicle Type", user.vehicleType),
//          profileColumn("Phone Number", user.phonenumber)


        ],
      ),
    ),
  );
  Widget followTopColumn(Size deviceSize, UserModel user) => Container(

    height: deviceSize.height * 0.13,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        ProfileTile(
          title: user.assigned.toString(),
          subtitle: "Assigned ",
        ),
        ProfileTile(
          title: user.pending.toString(),
          subtitle: "Pending",
        ),
        ProfileTile(
          title: user.completed.toString(),
          subtitle: "Completed ",
        ),

      ],
    ),
  );
  Widget bodyData(UserModel user) => SingleChildScrollView(

    child: Column(
      children: <Widget>[
        profileHeader(user.name, user.role),
        followTopColumn(deviceSize, user),

        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "Details",
            style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
          ),
        ),
       ListView(
         shrinkWrap: true,
         scrollDirection: Axis.vertical,
         physics: ScrollPhysics(),

         children: <Widget>[
           profileColumn("Email", user.email),

           profileColumn("Phone Number", user.phonenumber),
           profileColumn("Vehicle Type", user.vehicleType),
           profileColumn("Phone Number", user.vehicleNumber),



         ],
       )
       // postCard(user),
      ],
    ),
  );
//  Widget bodyData(UserModel user) => ListView(
//
//
//      children: <Widget>[
//        profileHeader(user.name, user.role),
//        followTopColumn(deviceSize),
//        postCard(user),
//      ],
//
//  );

  @override
  Widget build(BuildContext context) {

    //List userModel = Provider.of<UserModel>(context);
    deviceSize = MediaQuery.of(context).size;
    return CommonScaffold(
      appTitle: "Profile",
      bodyData:  FutureProvider(
        create: (context)=>userService.fetchUser(),
        catchError: (context, error){
          print(error.toString());
        },
        child: Consumer<UserModel>(
            builder: (context, user, widget){
                  //print( "swrer");
                  //print(order);
                  return (user==null)?Center(child: CircularProgressIndicator(),): bodyData(user)    ;

            }
        ) ,
      ), //(userModel==null)? Center(child: CircularProgressIndicator(),): bodyData(),
      elevation: 0.0,
    );
  }
}
