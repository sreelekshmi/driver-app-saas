import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';

class ProductDescriptionScreen extends StatefulWidget {
  String description;
  ProductDescriptionScreen(this.description);
  @override
  _ProductDescriptionScreenState createState() => _ProductDescriptionScreenState();
}

class _ProductDescriptionScreenState extends State<ProductDescriptionScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Description"),

      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: HtmlWidget(widget.description,
          webView: true,),
        ),
      )
//          Scrollable(
//            child: HtmlWidget(widget.description,
//            webView: true,
//
//            ),
//          ),

    );
  }
}
